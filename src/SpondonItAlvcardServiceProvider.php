<?php

namespace SpondonIt\AlvcardService;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Http\Kernel;
use SpondonIt\AlvcardService\Middleware\AlvcardService;

class SpondonItAlvcardServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $kernel = $this->app->make(Kernel::class);
        $kernel->pushMiddleware(AlvcardService::class);

        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'alvcard');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'alvcard');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }
}
