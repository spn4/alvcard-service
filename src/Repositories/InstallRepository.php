<?php

namespace SpondonIt\AlvcardService\Repositories;
ini_set('max_execution_time', -1);


use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Schema;
use SpondonIT\Aorapress\Models\Configuration\Config;
use SpondonIT\Aorapress\Repositories\Interfaces\Configuration\ConfigurationRepositoryInterface;
use SpondonIt\Service\Repositories\InstallRepository as ServiceInstallRepository;
use Throwable;

class InstallRepository {

    protected $installRepository;
	/**
	 * Instantiate a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(ServiceInstallRepository $installRepository) {
        $this->installRepository = $installRepository;
	}



	/**
	 * Install the script
	 */
	public function install($params) {

        try{

            if(!gbv($params, 'seed')){
                Artisan::call('db:seed', ['--class' => \Database\Seeders\RolePermission\RoleSeeder::class, '--force' => true]);
                Artisan::call('db:seed', ['--class' => \Database\Seeders\RolePermission\PermissionSeeder::class, '--force' => true]);
                Artisan::call('db:seed', ['--class' => \Database\Seeders\TableManageDataSeeder::class, '--force' => true]);
                Artisan::call('db:seed', ['--class' => \Database\Seeders\project\VcardTemplateSeeder::class, '--force' => true]);
                Artisan::call('db:seed', ['--class' => \Database\Seeders\project\PlanSeeder::class, '--force' => true]);
                Artisan::call('db:seed', ['--class' => \Database\Seeders\Settings\CurrencySeeder::class, '--force' => true]);
                Artisan::call('db:seed', ['--class' => \Database\Seeders\Settings\TimezoneSeeder::class, '--force' => true]);
                Artisan::call('db:seed', ['--class' => \Database\Seeders\Settings\LanguageSeeder::class, '--force' => true]);
                Artisan::call('db:seed', ['--class' => \Database\Seeders\Settings\NotificationSettingSeeder::class, '--force' => true]);
                Artisan::call('db:seed', ['--class' =>  \Database\Seeders\Appearance\ThemeSeeder::class, '--force' => true]);
                Artisan::call('db:seed', ['--class' => \Database\Seeders\VcardSystemDataSeeder::class, '--force' => true]);
                Artisan::call('db:seed', ['--class' => \Database\Seeders\project\DefaultPermissionSeeder::class, '--force' => true]);
                Artisan::call('db:seed', ['--class' => \Database\Seeders\project\FaqSeeder::class, '--force' => true]);
                Artisan::call('db:seed', ['--class' => \Database\Seeders\project\TestimonialSeeder::class, '--force' => true]);
                Artisan::call('db:seed', ['--class' => \Database\Seeders\Blog\BlogCategoryTableSeeder::class, '--force' => true]);
                Artisan::call('db:seed', ['--class' => \Database\Seeders\Blog\BlogPostTableSeeder::class, '--force' => true]);
                $config = App::make(ConfigurationRepositoryInterface::class);
                $config->setDefault();
                $config->getConfig();

            }
            $this->installRepository->seed(gbv($params, 'seed'));
            $admin = $this->makeAdmin($params);
            $this->postInstallScript($admin, $params);


            Artisan::call('key:generate', ['--force' => true]);

            envu([
                'APP_ENV' => 'production',
                'APP_DEBUG'     =>  'false',
                'ASSET_URL'     =>  app_url(),
            ]);



        } catch(\Exception $e){

            Storage::delete(['.user_email', '.user_pass']);

            throw ValidationException::withMessages(['message' => $e->getMessage()]);

        }
	}

	public function postInstallScript($admin, $params){
		Config::where('name', 'system_activated_date')->update(['text_value'=> date('Y-m-d')]);
		Config::where('name', 'system_domain')->update(['text_value'=> app_url()]);
	}




	/**
	 * Insert default admin details
	 */
	public function makeAdmin($params) {
        try{
            $user_model_name = config('spondonit.user_model');
            $user_class = new $user_model_name;
            $user = $user_class->find(1);
            if(!$user){
               $user = new $user_model_name;
            }
            $user->name = 'Super admin';
            $user->email = gv($params, 'email');
            if(Schema::hasColumn('users', 'role_id')){
                $user->role_id = 1;
            }
            if(\Illuminate\Support\Facades\Config::get('app.app_sync')){
                $user->password = bcrypt(12345678);

            }else{
                $user->password = bcrypt(gv($params, 'password', 'abcd1234'));
            }
            $user->guard='admin';
            $user->username='super';
            $user->email_verified_at=now();
            $user->save();
            $user->userPreference()->create([]);
            $user->assignRole('Super Admin');
        } catch(\Exception $e){
            $this->installRepository->rollbackDb();
            throw ValidationException::withMessages(['message' => $e->getMessage()]);
        }


	}

}
