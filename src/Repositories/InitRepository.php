<?php

namespace SpondonIt\AlvcardService\Repositories;

class InitRepository {

    public function init() {
		config([
            'app.item' => '30626608',
            'spondonit.module_manager_model' => \Modules\ModuleManager\Entities\InfixModuleManager::class,
            'spondonit.module_manager_table' => 'infix_module_managers',

            'spondonit.settings_model' => \SpondonIT\Aorapress\Models\Configuration::class,
            'spondonit.module_model' => \Nwidart\Modules\Facades\Module::class,

            'spondonit.user_model' => \SpondonIT\Aorapress\Models\User::class,
            'spondonit.settings_table' => 'configs',
            'spondonit.database_file' => '',
            'spondonit.saas_module_name' => '',
            'spondonit.support_multi_connection' => false,
              'spondonit.php_version' => '8.2.0'
        ]);
    }

    public function config()
	{

    }

}
